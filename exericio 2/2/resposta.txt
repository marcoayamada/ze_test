
-- for daily evolution
select 
	daily.DateID,
	product.ProductCategory
	sum(daily.StockQuantity) as sum_stock_qty,
from "Daily Inventory" daily
inner join "Product" product on daily.ProductID = product.ProductID
group by daily.DateID, product.ProductCategory

-- for monthly evolution (having many years)
select 
	product.ProductCategory,
	date.YearMonth
	sum(daily.StockQuantity) as sum_stock_qty,
from "Daily Inventory" daily
inner join "Product" product on daily.ProductID = product.ProductID
inner join "Date" date on daily.DateID = date.DateID
group by product.ProductCategory, date.YearMonth

-- if we want one month only, we have to do the same above
-- but grouping by Month (date.Month) and filter by month in where clause.

-------------------

select 
	daily.DateID,
	product.ProductCategory
	sum(daily.StockQuantity) as sum_stock_qt,
from "Daily Inventory" daily
inner join "Product" product on daily.ProductID = product.ProductID
where daily.DateID = (select max(DateID) from "Daily Inventory")
group by product.ProductCategory, daily.DateID

-------------------
with
base_query as (
	select
		product.ProductName,
		wh.State
	from "Daily Inventory" daily
	inner join "Product" product on daily.ProductID = product.ProductID
	inner join "WarehouseLocation" wh on daily.WarehouseID = wh.WarehouseID
)
select
	State,
	count(distinct ProductName)
from base_query
group by State
