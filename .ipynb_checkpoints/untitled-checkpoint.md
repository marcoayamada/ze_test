- Reason of bad rating can't be tracked. No values in dataset.
    - We can improve this to track what the reason of bad rating but we have to do without bias.
- Improve State column um product. People texting city names, state and other things

- Rating
    - 1, 2, 3, 4, 5: normal values
    - -1: skip rating
    - 0, blank: didn't receive
    - -1, 0, blank: don't compute to metric
    
    
- Quais fatores que estão baixando a média?
    - Não entrega
        - Período das 23 até as 8 não tem muitos pedidos e os que tem abaixam a média por conta de notas 1
        - Período das 9 as 22 tem mais pedidos mas também tem o mesmo problema
    - Cerveja quente
        - Presente em ambos períodos
    - Cerveja com espuma
        - Talvez, entregador sem o devido cuidado
        - Talvez cerveja quente
    - Frete caro
        - Não chega a ser uma nota 1, mas diminui. Fica no ranking 3 e 4

- Como está o treinamento desses estabelcimentos?
- Quem entrega está relacionado com o estabelcimento ou é alguém a parte que só entrega? O entregador pode não saber do treinamento se esse foi repassado
- Como é o treinamento? Seria bom destacar informação sobre pedidos na madrugada
    - Outra coisa interessante seria identificar os estabelecimentos que talvez não consigam dar um bom atendimento em algun horários
        
- Parece que no começo (poucos pedidos) o rating é baixo. Percebe-se que maior quantidade de pedidos a nota aumenta. Seria apenas no começo?


___
Ações
- Definir uma métrica para acompanhamento do rating
    - Média, NPS, outra?
    - Cenários mudam um pouco com média e NPS
    - Colocar um threshold para medir KPI em dimensões de tempo (hora, dia, semana, mes, etc), POC, Produto, etc